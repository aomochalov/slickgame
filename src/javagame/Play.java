package javagame;

import org.lwjgl.Sys;
import org.newdawn.slick.*;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

public class Play extends BasicGameState {

    Animation bucky, movingUp, movingDown, movingLeft, movingRight;
    Image worldMap;
    boolean quit = false;
    int[] duration = {200, 200};
    float buckyPositionX = 0;
    float buckyPositionY = 0;
    float shiftX = buckyPositionX + 320;
    float shiftY = buckyPositionY + 160;

    public Play(int play) {

    }

    @Override
    public int getID() {
        return 1;
    }

    @Override
    public void init(GameContainer gameContainer, StateBasedGame stateBasedGame) throws SlickException {
        worldMap = new Image("res/world.png");
        Image[] walkUp = {new Image("res/buckyBack.png"), new Image("res/buckyBack.png")};
        Image[] walkDown = {new Image("res/buckyFront.png"), new Image("res/buckyFront.png")};
        Image[] walkLeft = {new Image("res/buckyLeft.png"), new Image("res/buckyLeft.png")};
        Image[] walkRight = {new Image("res/buckyRight.png"), new Image("res/buckyRight.png")};

        movingUp = new Animation(walkUp, duration, false);
        movingDown = new Animation(walkDown, duration, false);
        movingLeft = new Animation(walkLeft, duration, false);
        movingRight = new Animation(walkRight, duration, false);
        bucky = movingDown;
    }

    @Override
    public void render(GameContainer gameContainer, StateBasedGame stateBasedGame, Graphics graphics) throws SlickException {
        worldMap.draw(buckyPositionX, buckyPositionY);
        bucky.draw(shiftX, shiftY);
        graphics.drawString("Bucky x: " + buckyPositionX + " y: " + buckyPositionY, 400, 20);

        if (quit) {
            graphics.drawString("Resume (R)", 250, 100);
            graphics.drawString("Main Menu (M)", 250, 150);
            graphics.drawString("Quit Game (Q)", 250, 200);

            if (!quit)
            graphics.clear();
        }
    }

    @Override
    public void update(GameContainer gameContainer, StateBasedGame stateBasedGame, int i) throws SlickException {
        Input input = gameContainer.getInput();

        if (input.isKeyDown(Input.KEY_UP)) {
            bucky = movingUp;
            buckyPositionY += i * .1f;
            if (buckyPositionY > 162) {
                buckyPositionY -= i * .1f;
            }
        }
        if (input.isKeyDown(Input.KEY_DOWN)) {
            bucky = movingDown;
            buckyPositionY -= i * .1f;
            if (buckyPositionY < -600) {
                buckyPositionY += i * .1f;
            }
        }
        if (input.isKeyDown(Input.KEY_LEFT)) {
            bucky = movingLeft;
            buckyPositionX += i * .1f;
            if (buckyPositionX > 324) {
                buckyPositionX -= i * .1f;
            }
        }
        if (input.isKeyDown(Input.KEY_RIGHT)) {
            bucky = movingRight;
            buckyPositionX -= i * .1f;
            if (buckyPositionX < -840) {
                buckyPositionX += i * .1f;
            }
        }

        if (input.isKeyDown(Input.KEY_ESCAPE)) {
            quit = true;
        }

        if (quit) {
            if (input.isKeyDown(Input.KEY_R)) {
                quit = false;
            }
            if (input.isKeyDown(Input.KEY_M)) {
                stateBasedGame.enterState(0);
            }
            if (input.isKeyDown(Input.KEY_Q)) {
                System.exit(0);
            }
        }
    }
}
