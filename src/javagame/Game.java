package javagame;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

public class Game extends StateBasedGame {

    public static final String gameName = "Ham Blaster";
    public static final int menu = 0;
    public static final int play = 1;

    public Game(String name) {
        super(name);
        this.addState(new Menu(menu));
        this.addState(new Play(play));
    }

    public static void main(String[] args) {
        AppGameContainer appGameContainer;
        try {
            appGameContainer = new AppGameContainer(new Game(gameName));
            appGameContainer.setDisplayMode(640, 320, false);
            appGameContainer.start();
        } catch (SlickException se) {
            se.printStackTrace();
        }
    }

    @Override
    public void initStatesList(GameContainer gameContainer) throws SlickException {
        this.getState(menu).init(gameContainer, this);
        this.getState(play).init(gameContainer, this);
        this.enterState(menu);
    }
}
