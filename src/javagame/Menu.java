package javagame;

import org.lwjgl.input.Mouse;
import org.newdawn.slick.*;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

public class Menu extends BasicGameState {

    Image playNow;
    Image exitGame;

    public Menu(int menu) {
    }

    @Override
    public int getID() {
        return 0;
    }

    @Override
    public void init(GameContainer gameContainer, StateBasedGame stateBasedGame) throws SlickException {
        playNow = new Image("res/playNow.png");
        exitGame = new Image("res/exitGame.png");
    }

    @Override
    public void render(GameContainer gameContainer, StateBasedGame stateBasedGame, Graphics graphics) throws SlickException {
        graphics.drawString("Welcome to Bucky Land!", 100, 50);
        playNow.draw(100, 100);
        exitGame.draw(100, 200);
    }

    @Override
    public void update(GameContainer gameContainer, StateBasedGame stateBasedGame, int i) throws SlickException {
        int posx = Mouse.getX();
        int posy = Mouse.getY();

        if ((posx > 100 && posx < 311) && (posy > 209 && posy < 260)) {
            if (Mouse.isButtonDown(0)) {
                stateBasedGame.enterState(1);
            }
        }

        if ((posx > 100 && posx < 311) && (posy > 109 && posy < 160)) {
            if (Mouse.isButtonDown(0)) {
                System.exit(0);
            }
        }
    }
}
